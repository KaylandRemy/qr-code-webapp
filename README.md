# QR Code WebApp

Explanation:  
  
This project will be a Web-App that will be able to scan a QR code to query a database a return a JSON file. This project is largely to expand my knowledge of web development and begin to learn the .Net framework and In addition QR codes development.
  
Goals:  
gain experience with .Net (in specific ASP.Net)  
gain experience with QR code development  
gain more experience with Databases  
Gain more experience with Git  
  
Tools (to be expanded as needed):  
IDE - Visual Studio  
Asp.net  
QR Code Encoder and Decoder .NET(Framework, Standard, Core) Class Library  
BitBucket  
SourceTree  
  
Links:  
https://www.codeproject.com/Articles/1250071/QR-Code-Encoder-and-Decoder-NET-Framework-Standard  
https://dotnet.microsoft.com/apps/aspnet  
https://bitbucket.org/KaylandRemy/qr-code-webapp/src/master/  
